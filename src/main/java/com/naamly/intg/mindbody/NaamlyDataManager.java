package com.naamly.intg.mindbody;

import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.db.*;
import com.naamly.intg.mindbody.db.naamly.*;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import com.naamly.intg.mindbody.util.NaamlyDataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;


@Component
public class NaamlyDataManager {

    @Autowired
    private NaamlyDataDAO naamlyDataDAO;

    @Autowired
    private ScheduleDAO scheduleDAO;

    public static final Logger logger = LoggerFactory.getLogger(NaamlyDataManager.class);


    public void populateDataTransaction(VisitsAtGlanceRecord visitsAtGlanceRecord, ClientAutoPayRecord autoPayRecord, StudioContext context) {
        try {
            logger.info("populateDataTransaction:: Populating record for client ");
            logger.info("Txn started ");
            JsonObject locationMap = context.getLocationMap();
            JsonObject trainerMap = context.getTrainerMap();
            NaamlyScheduleAtGlanceRecord scheduleAtGlanceRecord = NaamlyDataUtil.transformVisits(visitsAtGlanceRecord, locationMap, trainerMap);
            String firstName = scheduleAtGlanceRecord.getClientName().split(",")[0];
            String lastName = scheduleAtGlanceRecord.getClientName().split(",")[1];
            ClientRecord client = scheduleDAO.getClientByClientId(visitsAtGlanceRecord.getClientId());
            NaamlyClient naamlyClientRecord = naamlyDataDAO.getNaamlyClientByPhoneAndName(firstName, lastName);
            BigInteger newClientId = new BigInteger("0");

            if (naamlyClientRecord == null) {
                logger.info("There is no existing client found,so inserting new client record");
               naamlyDataDAO.saveClientRecord(NaamlyDataUtil.transformClient(scheduleAtGlanceRecord, client));
                NaamlyClient clientRecord1 = naamlyDataDAO.getNaamlyClientByPhoneAndName(firstName, lastName);
                if(clientRecord1!=null){
                    newClientId=clientRecord1.getId();
                }
            } else {
                newClientId = naamlyClientRecord.getId();
            }
            scheduleAtGlanceRecord.setClientId(newClientId);

            ClientMemberShipRecord clientMemberShipRecord = scheduleDAO.getMembershipByClientId(client.getClientId());
            NaamlyAutoExpiration clientAutoPayRecord = null;
            if (autoPayRecord != null) {
                clientAutoPayRecord = NaamlyDataUtil.transformAutoExp(scheduleAtGlanceRecord, autoPayRecord);
            }
            NaamlyCoachingHours coachingHours = NaamlyDataUtil.transformCoaching(scheduleAtGlanceRecord);
            NaamlyMemberShip memberShip = NaamlyDataUtil.transformMemberShip(scheduleAtGlanceRecord, clientMemberShipRecord);
            if (coachingHours.getCreatedDate() == null) {
                coachingHours.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            }
            naamlyDataDAO.saveCoachingHours(coachingHours);
            naamlyDataDAO.saveClientRecord(naamlyClientRecord);
            naamlyDataDAO.saveScheduleGlanceRecord(scheduleAtGlanceRecord);
            naamlyDataDAO.saveMemberShip(memberShip);

            if (clientAutoPayRecord != null) {
                naamlyDataDAO.saveAutoExpiration(clientAutoPayRecord);
            }

            logger.info("Txn ended");
        } catch (
                Exception ex) {
            logger.error("Found error while populating the Transaction exception {} ", ex);
        }


    }
}
