package com.naamly.intg.mindbody.orchestrator.studio;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.config.ConfigUtils;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.outbound.VisitsAtGlanceOutBoundController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Component
public class StudioWorkFlowManager {

    private static Logger logger = LoggerFactory.getLogger(StudioWorkFlowManager.class);
    @Autowired
    private ConfigUtils configUtils;

    @Autowired
    private StudioOrchestrator studioOrchestrator;
    private DateTimeFormatter startAndEndDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    private VisitsAtGlanceOutBoundController outBoundController;

    public boolean flow(String studioReferenceId, String studioName, int noOfDays) {
        try {
            JsonObject configData = configUtils.getMBConfiguration();
            if (Objects.isNull(studioReferenceId) || Objects.isNull(studioName)) {
                logger.debug("studioReferenceId is null,so return here ");
                return false;
            }
            String apiKey = configData.get("apiKey").getAsString();
            LocalDate now = LocalDate.now();
            LocalDate till = now.plusDays(noOfDays);
            String startDate = startAndEndDateFormat.format(now);
            String endDate = startAndEndDateFormat.format(till);
            StudioContext context = new StudioContext();
            context.setStudioId(studioReferenceId);
            context.setStudioName(studioName);
            context.setApiKey(apiKey);
            context.setStartDate(startDate);
            context.setEndDate(endDate);
            context.setStaffUserName(configData.get("staffUsername").getAsString());
            context.setStaffPwd(configData.get("staffPassword").getAsString());
            String staffToken=outBoundController.getStaffAccessToken(context);
            context.setStaffToken(staffToken);
            JsonArray studios = (JsonArray) configData.get("studios");
            for (int i=0;i<studios.size();i++){
                JsonObject studio=(JsonObject) studios.get(i);
                JsonObject siteId=(JsonObject)studio.get(studioReferenceId);
                if(siteId!=null){
                    JsonObject json=(JsonObject)studio.get(studioReferenceId);
                    context.setLocationMap((JsonObject) json.get("locationMapping"));
                    context.setTrainerMap((JsonObject) json.get("trainerMapping"));
                    break;
                }
            }
            studioOrchestrator.start(context);
            return true;
        } catch (Exception ex) {
            logger.error(" Found Exception in Studio Flow manager Exception : {} ", ex.getMessage());
            return false;
        }

    }
}
