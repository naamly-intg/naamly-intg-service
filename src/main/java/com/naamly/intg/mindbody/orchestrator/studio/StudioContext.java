package com.naamly.intg.mindbody.orchestrator.studio;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.orchestrator.BaseContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class StudioContext extends BaseContext {

    private Gson gson = new GsonBuilder().create();
    private DateFormat formatter =
            new SimpleDateFormat("YYYY-MM-DD'T'HH:mm:ss"); // "yyyy-MM-dd'T'HH:mm:ss");
    private DateFormat requestDateFormatter = new SimpleDateFormat("YYYY-MM-DD");
    private DateFormat sampleDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    public DateFormat getSampleDateFormatter() {
        return sampleDateFormatter;
    }
    private String startDate;
    private String endDate;
    private String studioName;
    private String studioId;
    private List<String> locationIds;
    private String apiKey;
    private List<JsonObject> classesJson;
    private JsonObject locationMap;
    private JsonObject trainerMap;
    private String staffUserName;
    private String staffPwd;

    public String getStaffToken() {
        return staffToken;
    }

    public void setStaffToken(String staffToken) {
        this.staffToken = staffToken;
    }

    private String staffToken;

    public String getStaffUserName() {
        return staffUserName;
    }

    public void setStaffUserName(String staffUserName) {
        this.staffUserName = staffUserName;
    }

    public String getStaffPwd() {
        return staffPwd;
    }

    public void setStaffPwd(String staffPwd) {
        this.staffPwd = staffPwd;
    }

    public JsonObject getLocationMap() {
        return locationMap;
    }

    public void setLocationMap(JsonObject locationMap) {
        this.locationMap = locationMap;
    }

    public JsonObject getTrainerMap() {
        return trainerMap;
    }

    public void setTrainerMap(JsonObject trainerMap) {
        this.trainerMap = trainerMap;
    }


    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }


    public void setVisitsJson(List<JsonObject> visitsJson) {
        this.visitsJson = visitsJson;
    }

    public List<JsonObject> getVisitsJson() {
        return visitsJson;
    }

    private List<JsonObject> visitsJson;

    public List<JsonObject> getClassesJson() {
        return classesJson;
    }

    public void setClassesJson(List<JsonObject> classesJson) {
        this.classesJson = classesJson;
    }

    public DateFormat getRequestDateFormatter() {
        return requestDateFormatter;
    }

    public void setRequestDateFormatter(DateFormat requestDateFormatter) {
        this.requestDateFormatter = requestDateFormatter;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getStudioName() {
        return studioName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public String getStudioId() {
        return studioId;
    }

    public void setStudioId(String studioId) {
        this.studioId = studioId;
    }

    public List<String> getLocationIds() {
        return locationIds;
    }

    public void setLocationIds(List<String> locationIds) {
        this.locationIds = locationIds;
    }

    public DateFormat getFormatter() {
        return formatter;
    }

    public Gson getGson() {
        return gson;
    }
}
