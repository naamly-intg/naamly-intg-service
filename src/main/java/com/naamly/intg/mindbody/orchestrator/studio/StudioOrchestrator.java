package com.naamly.intg.mindbody.orchestrator.studio;

import com.naamly.intg.mindbody.api.AppointmentAPIActivity;
import com.naamly.intg.mindbody.api.ClassesAPIActivity;
import com.naamly.intg.mindbody.api.NewMembersAPIActivity;
import com.naamly.intg.mindbody.api.VisitsAPIActivity;
import com.naamly.intg.mindbody.enricher.VisitAtGlanceEnricherActivity;
import com.naamly.intg.mindbody.extractor.ClassScheduleExtractorActivity;
import com.naamly.intg.mindbody.extractor.ClassVisitExtractorActivity;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseOrchestrator;
import com.naamly.intg.mindbody.orchestrator.studio.activity.CleanUpActivity;
import com.naamly.intg.mindbody.orchestrator.studio.activity.ClientLastVisitActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudioOrchestrator extends BaseOrchestrator {

  private static Logger logger = LoggerFactory.getLogger(StudioOrchestrator.class);

  @Autowired private ClassesAPIActivity classesAPIActivity;
  @Autowired private VisitsAPIActivity visitsAPIActivity;
  @Autowired private ClassScheduleExtractorActivity classScheduleExtractorActivity;
  @Autowired private ClassVisitExtractorActivity classVisitExtractorActivity;
  @Autowired private VisitAtGlanceEnricherActivity visitAtGlanceEnricherActivity;
  @Autowired private CleanUpActivity cleanUpActivity;
  @Autowired private ClientLastVisitActivity clientLastVisitActivity;
  @Autowired private NewMembersAPIActivity newMembersAPIActivity;
  @Autowired private AppointmentAPIActivity appointmentAPIActivity;

  public List<BaseActivity> getActivities() {
    logger.info("activities been called {} ", classesAPIActivity);
    List<BaseActivity> activities = new ArrayList<>();
    activities.add(appointmentAPIActivity);
   activities.add(newMembersAPIActivity);
    activities.add(cleanUpActivity);
    activities.add(classesAPIActivity);
    activities.add(classScheduleExtractorActivity);
    activities.add(visitsAPIActivity);
    activities.add(classVisitExtractorActivity);
    activities.add(visitAtGlanceEnricherActivity);
    activities.add(clientLastVisitActivity);
    return activities;
  }
}
