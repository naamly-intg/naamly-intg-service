package com.naamly.intg.mindbody.orchestrator.studio.activity;

import com.naamly.intg.mindbody.db.ClassVisitRecord;
import com.naamly.intg.mindbody.db.ClientLastVisitRecord;
import com.naamly.intg.mindbody.db.ScheduleDAO;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
@Component
public class ClientLastVisitActivity extends BaseActivity {

    private static Logger logger = LoggerFactory.getLogger(ClientLastVisitActivity.class);

    private boolean proceedToNextActivity = true;

    @Autowired
    private ScheduleDAO scheduleDAO;

    @Override
    public boolean proceedToNextActivity() {
        return proceedToNextActivity;
    }

    @Override
    public BaseContext execute(BaseContext context) {
        StudioContext studioContext = (StudioContext) context;
        logger.debug("LastDetailsVisitActivity.execute has been called ");
        List<ClassVisitRecord> classVisits = scheduleDAO.getAllClientVisitsByStudio(studioContext.getStudioId());
        Map<String, ClassVisitRecord> clientVsLastRecord = new HashMap<>();
        try {
            if (!Objects.isNull(classVisits) && classVisits.size() > 0) {
                for (ClassVisitRecord record : classVisits) {
                    if (clientVsLastRecord.get(record.getClientId()) == null) {
                        clientVsLastRecord.put(record.getClientId(), record);
                    } else {
                        ClassVisitRecord visitRecord = clientVsLastRecord.get(record.getClientId());
                        if (visitRecord.getLastVisitedDate().before(record.getLastVisitedDate())) {
                            clientVsLastRecord.put(record.getClientId(), record);
                        }
                    }
                }
            }
            for (Map.Entry<String, ClassVisitRecord> entry : clientVsLastRecord.entrySet()) {
                ClientLastVisitRecord record = new ClientLastVisitRecord();
                ClassVisitRecord visitRecord = entry.getValue();
                record.setClientId(visitRecord.getClientId());
                record.setVisitLocation(visitRecord.getLocationId());
                record.setClientName(visitRecord.getClientName());
                record.setBookingMethod(visitRecord.getBookingMethod());
                record.setPhone(visitRecord.getPhone());
                record.setEmail(visitRecord.getEmail());
                record.setLastVisitedAt(visitRecord.getLastVisitedDate());
                record.setServiceCategory(visitRecord.getServiceName());
                record.setLastUpdatedAt(new Timestamp(System.currentTimeMillis()));
                record.setStaff(visitRecord.getStaffName());
                record.setVisitType(visitRecord.getStudioId());
                logger.debug(" ClientVisitRecord {} ",record);
                scheduleDAO.createLastVisitRecord(record);
            }

        } catch (Exception ex) {
            logger.error("Found error in LastDetailsVisitActivity excetion {} ", ex);
        }

        return context;
    }
}
