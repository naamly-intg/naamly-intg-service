package com.naamly.intg.mindbody.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.db.ClientAutoPayRecord;
import com.naamly.intg.mindbody.db.ClientMemberShipRecord;
import com.naamly.intg.mindbody.db.ClientRecord;
import com.naamly.intg.mindbody.db.VisitsAtGlanceRecord;
import com.naamly.intg.mindbody.db.naamly.*;
import com.naamly.intg.mindbody.orchestrator.studio.StudioWorkFlowManager;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NaamlyDataUtil {

    private static Logger logger = LoggerFactory.getLogger(StudioWorkFlowManager.class);


    public static NaamlyScheduleAtGlanceRecord transformVisits(VisitsAtGlanceRecord inRecord, JsonObject locationMap, JsonObject trainerMap) {
        NaamlyScheduleAtGlanceRecord outRecord = new NaamlyScheduleAtGlanceRecord();
        try {
            JsonElement locationId = locationMap.get(inRecord.getLocationId());
            if (locationId != null && !locationId.isJsonNull()) {
                outRecord.setLocationId(new BigInteger(locationId.getAsString()));
            }
            JsonElement trainerId = trainerMap.get(inRecord.getLocationId());
            if (trainerId != null && !trainerId.isJsonNull()) {
                outRecord.setTrainerId(new BigInteger(trainerId.getAsString()));
            }
            outRecord.setStartTime(inRecord.getStartTime());
            outRecord.setEndTime(inRecord.getEndTime());
            outRecord.setDescription(inRecord.getClassDescription());
            outRecord.setStaff(inRecord.getStaffDetails());
            outRecord.setNotes(inRecord.getNotes());
            outRecord.setClientName(inRecord.getClientName());
            if (inRecord.getRemaining() != null) {
                outRecord.setRemaining(Integer.parseInt(inRecord.getRemaining()));
            }
            try {
                String phone = inRecord.getPhone();
                phone = "+1" + phone.replaceAll("[^\\d]", "");
                outRecord.setPhone(phone);
            } catch (Exception e) {
                logger.info("phone ex - " + e.getMessage());
            }

            outRecord.setPhone(inRecord.getPhone());
            outRecord.setScheduledOnline(inRecord.getIsScheduledOnline());
            outRecord.setStatus(inRecord.getStatus());
            outRecord.setMemberShip(inRecord.getMemberShipKind());
            outRecord.setStaffAlert(inRecord.getStaffAlert());
            outRecord.setYellowAlert(inRecord.getYellowAlert());
            outRecord.setAppointmentNotes(inRecord.getAppointmentNotes());
            outRecord.setFirstVisit(inRecord.getIsFirstVisit());
            if (inRecord.getBirthDate() != null) {
                outRecord.setBirthDay(inRecord.getBirthDate().toString());
            }
            outRecord.setLastVisitDate(inRecord.getLastVisitDate());
            outRecord.setEmailAddress(inRecord.getEmail());
            outRecord.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            return outRecord;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //TODO
    public static NaamlyClient transformClient(NaamlyScheduleAtGlanceRecord record, ClientRecord clientRecord) {
        NaamlyClient naamlyClient = new NaamlyClient();
        naamlyClient.setTrainerId(record.getTrainerId());
        naamlyClient.setLocationId(record.getLocationId());
        String clientName = clientRecord.getClientName();
        String firstName = clientName.split(",")[0];
        String lastName = clientName.split(",")[1];
        naamlyClient.setFirstName(firstName);
        naamlyClient.setLastName(lastName);
        naamlyClient.setPhone(clientRecord.getPhoneNumber());
        naamlyClient.setEmailId(clientRecord.getEmail());
        naamlyClient.setDob(new Date());
        naamlyClient.setEmailId(clientRecord.getEmail());
        naamlyClient.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        naamlyClient.setHeightUnit("cm");
        naamlyClient.setStatus("Y");
        naamlyClient.setStartDate(new Date());
        naamlyClient.setStatusOverride(1);
        naamlyClient.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        return naamlyClient;
    }

    public static NaamlyMemberShip transformMemberShip(NaamlyScheduleAtGlanceRecord record, ClientMemberShipRecord memberShipRecord) {
        NaamlyMemberShip membership = new NaamlyMemberShip();
        membership.setName(record.getClientName());
        membership.setTrainerId(record.getTrainerId());
        membership.setLocationId(record.getLocationId());
        if (memberShipRecord != null) {
            if (memberShipRecord.getStatus() == null) {
                membership.setStatus("Not found");
            } else {
                membership.setStatus(memberShipRecord.getStatus());
            }
            membership.setMembershipType(memberShipRecord.getMemberShipType());
            if (memberShipRecord.getMemberFrom() != null) {
                membership.setMemberFrom(memberShipRecord.getMemberFrom().toString());
            }
            if (memberShipRecord.getMemberTo() != null) {
                membership.setMemberTo(memberShipRecord.getMemberTo().toString());
            }
            membership.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        }
        return membership;
    }


    public static NaamlyAutoExpiration transformAutoExp(NaamlyScheduleAtGlanceRecord record, ClientAutoPayRecord autoPayRecord) {
        NaamlyAutoExpiration autoPay = new NaamlyAutoExpiration();
        autoPay.setTrainerId(record.getTrainerId());
        autoPay.setLocationId(record.getLocationId());
        autoPay.setClient(record.getClientName());
        autoPay.setAmount(autoPayRecord.getAmount());
        autoPay.setLocationId(record.getLocationId());
        autoPay.setLastAutoPayDate("2020-08-20");//ToDo
        autoPay.setCreatedAt(new Date());
        return autoPay;
    }

    public static NaamlyCoachingHours transformCoaching(NaamlyScheduleAtGlanceRecord record) {
        NaamlyCoachingHours coachingHours = new NaamlyCoachingHours();
        try {
            coachingHours.setLocationId(record.getLocationId());
            coachingHours.setClientName(record.getClientName());
            coachingHours.setDate(transformCoachingHoursDate(record.getBirthDay()));
            coachingHours.setStaff(record.getStaff());
            coachingHours.setStartTime(record.getStartTime());
            coachingHours.setEndTime(record.getEndTime());
            coachingHours.setPhone(record.getPhone());
            coachingHours.setEmail(record.getEmailAddress());
            coachingHours.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            return coachingHours;
        } catch (Exception ex) {
            logger.error("Found exception while transforming the Coaching hours record {} ", ex);
        }
        return coachingHours;
    }

    private static String transformCoachingHoursDate(String Birthday) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
        SimpleDateFormat newformat = new SimpleDateFormat("M/d/yyyy");
        Date date = null;
        try {
            date = sdf.parse(Birthday);
        } catch (Exception e) {
            logger.error("Found exception while formatting the date {} ", e);
        }
        String formated_date = newformat.format(date);
        return formated_date;
    }


}
