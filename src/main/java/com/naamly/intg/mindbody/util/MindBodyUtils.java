package com.naamly.intg.mindbody.util;

import com.naamly.intg.mindbody.db.ClassScheduleRecord;
import com.naamly.intg.mindbody.db.ClassVisitRecord;
import com.naamly.intg.mindbody.db.VisitsAtGlanceRecord;
import com.naamly.intg.mindbody.enricher.VisitAtGlanceEnricherActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MindBodyUtils {

    private static Logger log = LoggerFactory.getLogger(MindBodyUtils.class);

    public static List<VisitsAtGlanceRecord> transformVisitsAtGlance(
            ClassScheduleRecord classScheduleRecord, List<ClassVisitRecord> visitsRecords) {
        log.info("transformVisitsAtGlance has been called ");
        List<VisitsAtGlanceRecord> visitsAtGlanceRecords = new ArrayList<>();
        try {
            for (ClassVisitRecord record : visitsRecords) {
                VisitsAtGlanceRecord visitsAtGlanceRecord = new VisitsAtGlanceRecord();
                visitsAtGlanceRecord.setStudioId(classScheduleRecord.getStudioId());
                visitsAtGlanceRecord.setLocationId(classScheduleRecord.getLocationId());
                visitsAtGlanceRecord.setClassId(classScheduleRecord.getClassId());
                visitsAtGlanceRecord.setScheduledDate(classScheduleRecord.getScheduledDate());
                visitsAtGlanceRecord.setStartTime(classScheduleRecord.getStartTime());
                visitsAtGlanceRecord.setEndTime(classScheduleRecord.getEndTime());
                visitsAtGlanceRecord.setClassDescription(classScheduleRecord.getClassDescription());
                visitsAtGlanceRecord.setStaffDetails(classScheduleRecord.getStaffDetails());
                visitsAtGlanceRecord.setNotes(classScheduleRecord.getNotes());
                visitsAtGlanceRecord.setIsScheduledOnline(classScheduleRecord.getIsScheduledOnline());
                visitsAtGlanceRecord.setStaffAlert(classScheduleRecord.getStaffAlert());
                visitsAtGlanceRecord.setYellowAlert(classScheduleRecord.getYellowAlert());
                visitsAtGlanceRecord.setCreatedAt(classScheduleRecord.getCreatedAt());
                visitsAtGlanceRecord.setLastUpdatedAt(classScheduleRecord.getLastUpdatedAt());
                visitsAtGlanceRecord.setClientId(record.getClientId());
                visitsAtGlanceRecord.setClientName(record.getClientName());
                visitsAtGlanceRecord.setPhone(record.getPhone());
                visitsAtGlanceRecord.setStatus(record.getStatus());
                visitsAtGlanceRecord.setMemberShipKind(record.getMemberShipKind());
                visitsAtGlanceRecord.setAppointmentNotes(record.getAppointmentNotes());
                visitsAtGlanceRecord.setIsUnPaidAppointment(record.getIsUnPaidAppointment());
                visitsAtGlanceRecord.setIsFirstVisit(record.getIsFirstVisit());
                visitsAtGlanceRecord.setBirthDate(record.getBirthDate());
                visitsAtGlanceRecord.setRemaining(record.getRemaining());
                visitsAtGlanceRecord.setMemberShipKind(record.getMemberShipKind());
                visitsAtGlanceRecords.add(visitsAtGlanceRecord);
            }
        } catch (Exception ex) {
            log.error(" Found exception while preparing glance record. ex {} ", ex);
        }

        return visitsAtGlanceRecords;
    }

    public static Timestamp getDateFromDateString(String date, DateFormat format) throws ParseException {
        String actualDate = date.split("T")[0];
        return new Timestamp(format.parse(actualDate).getTime());
    }
}
