package com.naamly.intg.mindbody.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.db.ClientAutoPayRecord;
import com.naamly.intg.mindbody.db.ClientMemberShipRecord;
import com.naamly.intg.mindbody.db.ClientRecord;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Objects;

public class JsonExtractor {

    private static Logger log = LoggerFactory.getLogger(JsonExtractor.class);


    public static ClientAutoPayRecord extractAutoPay(JsonObject autoPayDetails, String clientId, String clientName, StudioContext studioContext) {
        log.debug("JsonExtractor.extractAutoPay clientId {}  ", clientId);
        try {
            JsonArray contracts = autoPayDetails.getAsJsonArray("Contracts");
            if (!Objects.isNull(contracts) && contracts.size() > 0) {
                int lastContractIndex = contracts.size() - 1;
                JsonObject lastContract = (JsonObject) contracts.get(lastContractIndex);
                JsonArray upcomingAutopayEvents = (JsonArray) lastContract.get("UpcomingAutopayEvents");
                if(upcomingAutopayEvents!=null && upcomingAutopayEvents.size()==0){
                    return null;
                }
                JsonObject lastAutoPayEvent = (JsonObject) upcomingAutopayEvents.get(upcomingAutopayEvents.size() - 1);
                if (lastAutoPayEvent != null) {
                    ClientAutoPayRecord autoPayRecord = new ClientAutoPayRecord();
                    if (!lastAutoPayEvent.get("ChargeAmount").isJsonNull()) {
                        autoPayRecord.setAmount(lastAutoPayEvent.get("ChargeAmount").getAsString());
                        autoPayRecord.setClientId(clientId);
                        autoPayRecord.setClientName(clientName);
                        autoPayRecord.setCreatedAt(new Timestamp(System.currentTimeMillis()));
                        autoPayRecord.setLastUpdatedAt(new Timestamp(System.currentTimeMillis()));
                        if (!lastAutoPayEvent.get("ScheduleDate").isJsonNull()) {
                            Timestamp stamp = MindBodyUtils.getDateFromDateString(lastAutoPayEvent.get("ScheduleDate").getAsString(), studioContext.getSampleDateFormatter());
                            autoPayRecord.setLastAutoPay(stamp);
                        }
                        return autoPayRecord;
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Found error while extracting the autoPay {} ", ex);
        }
        return null;
    }

    public static ClientRecord extractClient(String clientId, JsonArray clients, StudioContext studioContext) {
        log.debug("JsonExtractor.extractClient ");
        try {
            JsonObject clientInfo = (JsonObject) clients.get(0);
            if (!Objects.isNull(clientInfo)) {
                ClientRecord clientRecord = new ClientRecord();
                clientRecord.setClientId(clientId);
                String clientName =
                        clientInfo.get("FirstName").getAsString()
                                + ","
                                + clientInfo.get("LastName").getAsString();
                //record.setClientName(clientName);
                clientRecord.setClientName(clientName);
                if (!clientInfo.get("CreationDate").isJsonNull()) {
                    String memberFrom = clientInfo.get("CreationDate").getAsString();
                    String actualDate = memberFrom.split("T")[0];
                    Timestamp ts =
                            new Timestamp(
                                    studioContext.getSampleDateFormatter().parse(actualDate).getTime());
                    clientRecord.setCreatedAt(ts);
                }

                if (!clientInfo.get("BirthDate").isJsonNull()) {
                    String birthDate = clientInfo.get("BirthDate").getAsString();
                    String actualDate = birthDate.split("T")[0];
                    Timestamp ts =
                            new Timestamp(
                                    studioContext.getSampleDateFormatter().parse(actualDate).getTime());
                    clientRecord.setBirthDate(ts);
                    //record.setBirthDate(ts);
                }
                if (!Objects.isNull(clientInfo.get("Email").isJsonNull())) {
                    clientRecord.setEmail(clientInfo.get("Email").getAsString());
                    // record.setEmail(clientInfo.get("Email").getAsString());
                }
                if (!clientInfo.get("MobilePhone").isJsonNull()) {
                    clientRecord.setPhoneNumber(clientInfo.get("MobilePhone").getAsString());
                }
                clientRecord.setCreatedAt(new Timestamp(System.currentTimeMillis()));
                clientRecord.setLastUpdatedAt(new Timestamp(System.currentTimeMillis()));
                return clientRecord;
            }
        } catch (Exception ex) {
            log.error("Found error while extracting the Client {} ", ex);
        }
        return null;
    }

    public static ClientMemberShipRecord extractMemberShipRecord(ClientRecord clientRecord, JsonObject activeClientMemberShipDetails, StudioContext studioContext) {
        try {
            ClientMemberShipRecord clientMemberShipRecord = new ClientMemberShipRecord();
            clientMemberShipRecord.setClientId(clientRecord.getClientId());
            clientMemberShipRecord.setClientName(clientRecord.getClientName());
            clientMemberShipRecord.setMemberFrom(clientRecord.getCreatedAt());
            clientMemberShipRecord.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            clientMemberShipRecord.setLastUpdatedAt(new Timestamp(System.currentTimeMillis()));
            if (!Objects.isNull(activeClientMemberShipDetails)) {
                JsonArray memberships = (JsonArray) activeClientMemberShipDetails.get("ClientMemberships");
                if (!Objects.isNull(memberships) && memberships.size()>0) {
                    JsonObject item = (JsonObject) memberships.get(0);
                    if (!Objects.isNull(item)) {
                        if (!Objects.isNull(item.get("Remaining"))) {
                            String remaining = item.get("Remaining").getAsString();
                            clientMemberShipRecord.setRemaining(remaining);
                        }
                        if (!item.get("ActiveDate").isJsonNull()) {
                            String memberFrom = item.get("ActiveDate").getAsString();
                            String actualDate = memberFrom.split("T")[0];
                            Timestamp ts =
                                    new Timestamp(
                                            studioContext.getSampleDateFormatter().parse(actualDate).getTime());
                            clientMemberShipRecord.setMemberFrom(ts);
                        }

                        if (!item.get("ExpirationDate").isJsonNull()) {
                            String memberFrom = item.get("ExpirationDate").getAsString();
                            String actualDate = memberFrom.split("T")[0];
                            Timestamp ts =
                                    new Timestamp(
                                            studioContext.getSampleDateFormatter().parse(actualDate).getTime());
                            clientMemberShipRecord.setMemberTo(ts);
                        }
                        if (!item.get("Name").isJsonNull()) {
                            clientMemberShipRecord.setMemberShipType(item.get("Name").getAsString());
                        }
                    }
                }
            }
            if(clientMemberShipRecord.getMemberShipType()==null||clientMemberShipRecord.getMemberShipType().isEmpty()){
                clientMemberShipRecord.setMemberShipType("Member");
            }
            return clientMemberShipRecord;
        } catch (Exception ex) {
            log.error("Found Exception while extractMemberShipRecord {} ", ex);
        }
        return null;
    }
}
