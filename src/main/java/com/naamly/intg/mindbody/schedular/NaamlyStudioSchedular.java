package com.naamly.intg.mindbody.schedular;

import com.naamly.intg.mindbody.MindBodyEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@EnableScheduling
@Component
public class NaamlyStudioSchedular {

    @Autowired
    private MindBodyEndpoint endpoint;

    /**
     * perform for life
     * every Day 12PM
     */
    @Scheduled(cron = "0 0 0 * * *")
    public void performForLifeScheduling() {
        endpoint.prepareStudio("147502", "Perform for Life", 2);
    }

    /**
     * Hanley Strength Systems
     * configured every day 10PM
     */
    @Scheduled(cron = "0 10 * * * *")
    public void hanleyScheduling() {
        endpoint.prepareStudio("154134", "Hanley Strength Systems", 2);
    }

}
