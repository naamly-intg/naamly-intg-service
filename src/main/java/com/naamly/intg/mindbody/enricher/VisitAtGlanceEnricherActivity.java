package com.naamly.intg.mindbody.enricher;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.NaamlyDataManager;
import com.naamly.intg.mindbody.config.ConfigUtils;
import com.naamly.intg.mindbody.db.*;
import com.naamly.intg.mindbody.util.MindBodyUtils;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Component
public class VisitAtGlanceEnricherActivity extends BaseActivity {

  @Autowired private ScheduleDAO scheduleDAO;
  private static Logger log = LoggerFactory.getLogger(VisitAtGlanceEnricherActivity.class);
  private boolean proceedToNextActivity=true;

  @Autowired
  private NaamlyDataManager naamlyDataManager;



  @Override
  public BaseContext execute(BaseContext context) {
    try {
      StudioContext studioContext = (StudioContext)context;

      log.info("VisitAtGlanceEnricherActivity activity started ");
      List<ClassScheduleRecord> classScheduleRecordList =
          scheduleDAO.getClassesBySiteId(studioContext.getStudioId());
      for (ClassScheduleRecord record : classScheduleRecordList) {
        List<ClassVisitRecord> visitRecords = scheduleDAO.getClientsByClassId(record.getClassId());
        List<VisitsAtGlanceRecord> visitsAtGlanceRecords =
                MindBodyUtils.transformVisitsAtGlance(record, visitRecords);
        if (!Objects.isNull(visitsAtGlanceRecords)) {
          for (VisitsAtGlanceRecord visitsAtGlanceRecord : visitsAtGlanceRecords) {
            scheduleDAO.createVisitsAtGlanceRecord(visitsAtGlanceRecord);
            ClientAutoPayRecord autoPayRecord=scheduleDAO.getClientAutoPayRecord(visitsAtGlanceRecord.getClientId());
            log.info("Populating actual naamly tables ");
            naamlyDataManager.populateDataTransaction(visitsAtGlanceRecord,autoPayRecord,studioContext);
          }
        }
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return context;
  }

  @Override
  public boolean proceedToNextActivity() {
    return proceedToNextActivity;
  }
}
