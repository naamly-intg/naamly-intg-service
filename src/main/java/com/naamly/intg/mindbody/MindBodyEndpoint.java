package com.naamly.intg.mindbody;

import com.naamly.intg.mindbody.orchestrator.studio.StudioWorkFlowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/1.0/mb")
public class MindBodyEndpoint {

    @Autowired
    private StudioWorkFlowManager studioWorkFlowManager;

    private static Logger logger = LoggerFactory.getLogger(StudioWorkFlowManager.class);


    @PostMapping(value = "/studio")
    public void prepareStudio(@RequestParam String studioReferenceId, @RequestParam String studioName, @RequestParam int noOfDays) {
        try {
            studioWorkFlowManager.flow(studioReferenceId, studioName, noOfDays);
        } catch (Exception ex) {
            logger.error("Found Exception while fetching data for studio {} ", studioName);
            ex.printStackTrace();
        }
    }

}
