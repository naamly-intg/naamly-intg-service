package com.naamly.intg.mindbody.extractor;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.NaamlyDataManager;
import com.naamly.intg.mindbody.db.*;
import com.naamly.intg.mindbody.db.naamly.NaamlyDataDAO;
import com.naamly.intg.mindbody.outbound.VisitsAtGlanceOutBoundController;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import com.naamly.intg.mindbody.util.JsonExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Component
public class ClassVisitExtractorActivity extends BaseActivity {

    private boolean proceedToNextActivity = true;
    @Autowired
    private ScheduleDAO scheduleDAO;

    @Autowired
    private VisitsAtGlanceOutBoundController outBoundController;


    private static Logger log = LoggerFactory.getLogger(ClassVisitExtractorActivity.class);

    @Override
    public boolean proceedToNextActivity() {
        return proceedToNextActivity;
    }

    @Override
    public BaseContext execute(BaseContext context) {
        try {
            StudioContext studioContext = (StudioContext) context;
            List<JsonObject> visitsJson = studioContext.getVisitsJson();
            if (Objects.isNull(visitsJson)) {
                return studioContext;
            }
            for (int i = 0; i < visitsJson.size(); i++) {
                JsonObject visitJson = visitsJson.get(i);

                try {
                    JsonObject aClass = (JsonObject) visitJson.get("Class");
                    if (Objects.isNull(aClass) || aClass.get("Visits") == null) {
                        continue;
                    }
                    JsonArray visits = aClass.get("Visits").getAsJsonArray();
                    if (visits.size() == 0) {
                        continue;
                    }
                    log.info("clientArray = {} ", visits);
                    for (int j = 0; j < visits.size(); j++) {
                        JsonObject client = (JsonObject) visits.get(j);
                        ClassVisitRecord record = new ClassVisitRecord();
                        record.setClassId(aClass.get("Id").getAsString());
                        String clientId = client.get("ClientId").getAsString();
                        record.setClientId(clientId);
                        record.setStudioId(studioContext.getStudioId());
                        JsonObject locationJson = (JsonObject) aClass.get("Location");
                        record.setLocationId(locationJson.get("Id").getAsString());
                        if (!client.get("EndDateTime").isJsonNull()) {
                            String memberFrom = client.get("EndDateTime").getAsString();
                            String actualDate = memberFrom.split("T")[0];
                            Timestamp ts =
                                    new Timestamp(
                                            studioContext.getSampleDateFormatter().parse(actualDate).getTime());
                            record.setLastVisitedDate(ts);
                        }
                        if (!client.get("ServiceName").isJsonNull()) {
                            record.setServiceName(client.get("ServiceName").getAsString());
                        }
                        if (!client.get("WebSignup").isJsonNull()) {
                            boolean isWebSignUp = client.get("WebSignup").getAsBoolean();
                            if (isWebSignUp) {
                                record.setBookingMethod("WEB");
                            } else {
                                record.setBookingMethod("NON-WEB");
                            }
                        }
                        JsonObject staff = (JsonObject) aClass.get("Staff");
                        if (!Objects.isNull(staff)) {
                            String staffName = staff.get("FirstName").getAsString() + "," + staff.get("LastName").getAsString();
                            record.setStaffName(staffName);
                        }
                        JsonObject autoPayDetails = outBoundController.getAutoPayDetails(clientId, studioContext);
                        ClientRecord clientRecord = scheduleDAO.getClientByClientId(clientId);
                        if (clientRecord == null) {
                            JsonObject clientJson = outBoundController.getClientInfo(clientId, studioContext);
                            if (clientJson == null) {
                                continue;
                            }
                            JsonArray clients = (JsonArray) clientJson.get("Clients");
                            if (clients != null && clients.size() > 0) {
                                clientRecord = JsonExtractor.extractClient(clientId, clients, studioContext);
                            }
                        }
                        ClientAutoPayRecord autoPayRecord = null;
                        if (!Objects.isNull(clientRecord)) {
                            if (autoPayDetails != null) {
                                log.info("autoPay details= {} ", autoPayDetails);
                                autoPayRecord = JsonExtractor.extractAutoPay(autoPayDetails, clientId, clientRecord.getClientName(), studioContext);
                                if (!Objects.isNull(autoPayRecord)) {
                                    scheduleDAO.createClientAutoPayRecord(autoPayRecord);
                                }
                            }
                            record.setClientName(clientRecord.getClientName());
                            JsonObject activeClientMemberShipDetails = outBoundController.getActiveClientMemberShipDetails(clientId, studioContext);
                            ClientMemberShipRecord clientMemberShipRecord = JsonExtractor.extractMemberShipRecord(clientRecord, activeClientMemberShipDetails, studioContext);
                            record.setRemaining(clientMemberShipRecord.getRemaining());
                            record.setMemberShipKind(clientMemberShipRecord.getMemberShipType());
                            clientMemberShipRecord.setRemaining(clientMemberShipRecord.getRemaining());
                            record.setPhone(clientRecord.getPhoneNumber());
                            scheduleDAO.createClientRecord(clientRecord);
                            scheduleDAO.createClientMembershipRecord(clientMemberShipRecord);
                            scheduleDAO.createClassVisitRecord(record);
                        }
                    }

                } catch (Exception ex) {
                    log.error("Found error while processing  visit {} exception {}",visitJson,ex);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return context;
    }
}
