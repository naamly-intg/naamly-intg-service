package com.naamly.intg.mindbody.api;

import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.config.ConfigUtils;
import com.naamly.intg.mindbody.outbound.VisitsAtGlanceOutBoundController;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.*;

@Component
public class ClassesAPIActivity extends BaseActivity {

    private static Logger logger = LoggerFactory.getLogger(ClassesAPIActivity.class);

    @Autowired
    private VisitsAtGlanceOutBoundController visitsAtGlanceOutBoundController;

    private boolean proceedToNextActivity = true;

    @Autowired
    private ConfigUtils configUtils;

    @Override
    public boolean proceedToNextActivity() {
        return proceedToNextActivity;
    }

    @Override
    public BaseContext execute(BaseContext context) {
        try {
            StudioContext studioContext = (StudioContext) context;
            logger.debug("ClassesAPIEndPoint.call has been called ");


            String body =
                    visitsAtGlanceOutBoundController.getClassInfo(
                            studioContext.getStartDate(), studioContext.getEndDate(), true, 200, 0, studioContext);
            if (!Objects.isNull(body)) {
                JsonObject responseJson = studioContext.getGson().fromJson(body, JsonObject.class);
                if (!Objects.isNull(responseJson)) {
                    logger.info("responseJson:: " + responseJson);
                    JsonObject pagination = (JsonObject) responseJson.get("PaginationResponse");

                    List<JsonObject> classesResponse = new ArrayList<>();

                    int totalClasses = pagination.get("TotalResults").getAsInt();
                    logger.info("Total classes are {} ", totalClasses);
                    int offSet = 0;
                    int limit = 200;
                    while (offSet < totalClasses) {
                        String pageResponse =
                                visitsAtGlanceOutBoundController.getClassInfo(
                                        studioContext.getStartDate(), studioContext.getEndDate(), true, limit, offSet, studioContext);
                        if (!Objects.isNull(pageResponse)) {
                            classesResponse.add(studioContext.getGson().fromJson(pageResponse, JsonObject.class));
                        }
                        offSet = offSet + limit;
                    }
                    studioContext.setClassesJson(classesResponse);
                }
            }

        } catch (Exception ex) {
            logger.error("Found error while fetching ClassesAPIActivity exception is {} ", ex);
        }
        return context;
    }
}
