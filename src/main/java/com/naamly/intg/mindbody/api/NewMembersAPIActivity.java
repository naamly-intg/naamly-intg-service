package com.naamly.intg.mindbody.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.config.ConfigUtils;
import com.naamly.intg.mindbody.db.ClientNewMemberShipRecord;
import com.naamly.intg.mindbody.db.ScheduleDAO;
import com.naamly.intg.mindbody.db.naamly.NaamlyDataDAO;
import com.naamly.intg.mindbody.db.naamly.NaamlyNewMembersReport;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import com.naamly.intg.mindbody.outbound.VisitsAtGlanceOutBoundController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class NewMembersAPIActivity extends BaseActivity {

    private static Logger logger = LoggerFactory.getLogger(ClassesAPIActivity.class);

    @Autowired
    private VisitsAtGlanceOutBoundController visitsAtGlanceOutBoundController;

    @Autowired
    private ScheduleDAO scheduleDAO;

    @Autowired
    private NaamlyDataDAO naamlyDataDAO;

    private boolean proceedToNextActivity = true;

    @Autowired
    private ConfigUtils configUtils;

    @Override
    public boolean proceedToNextActivity() {
        return proceedToNextActivity;
    }

    @Override
    public BaseContext execute(BaseContext context) {
        try {
            StudioContext studioContext = (StudioContext) context;
            logger.debug("NewMembersAPIActivity.call has been called ");

            String body =
                    visitsAtGlanceOutBoundController.getClientsByCreatedDate(
                            studioContext.getStartDate(), studioContext);
            if (!Objects.isNull(body)) {
                JsonObject responseJson = studioContext.getGson().fromJson(body, JsonObject.class);
                if (!Objects.isNull(responseJson)) {
                    logger.info("responseJson:: " + responseJson);
                    JsonObject pagination = (JsonObject) responseJson.get("PaginationResponse");

                    int totalClasses = pagination.get("TotalResults").getAsInt();
                    logger.info("new members details from start date {}  to current date  ", studioContext.getStartDate());
                    int offSet = 0;
                    int limit = 200;
                    while (offSet < totalClasses) {
                        String pageResponse =
                                visitsAtGlanceOutBoundController.getClientsByCreatedDate(
                                        studioContext.getStartDate(), limit, offSet, studioContext);
                        if (!Objects.isNull(pageResponse)) {
                            JsonObject json = studioContext.getGson().fromJson(pageResponse, JsonObject.class);
                            JsonArray array = (JsonArray) json.get("Clients");
                            for (int i = 0; i < array.size(); i++) {
                                JsonObject js = (JsonObject) array.get(i);
                                if (js != null) {
                                    ClientNewMemberShipRecord newClient = new ClientNewMemberShipRecord();
                                    newClient.setClientId(js.get("Id").getAsString());

                                    newClient.setClientName(js.get("FirstName").getAsString()+","+js.get("LastName").getAsString());
                                    newClient.setMemberShipName("New");
                                    if (js.get("Phone") != null) {
                                        newClient.setPhone(js.get("Phone").getAsString());
                                    }
                                    if (!js.get("CreationDate").isJsonNull()) {
                                        String memberFrom = js.get("CreationDate").getAsString();
                                        String actualDate = memberFrom.split("T")[0];
                                        Timestamp ts =
                                                new Timestamp(
                                                        studioContext.getSampleDateFormatter().parse(actualDate).getTime());
                                        newClient.setDateCreated(ts);
                                        newClient.setDateJoined(ts);
                                    }

                                    if (!js.get("LastModifiedDateTime").isJsonNull()) {
                                        String memberFrom = js.get("LastModifiedDateTime").getAsString();
                                        String actualDate = memberFrom.split("T")[0];
                                        Timestamp ts =
                                                new Timestamp(
                                                        studioContext.getSampleDateFormatter().parse(actualDate).getTime());
                                        newClient.setLastUpdatedAt(ts);
                                    }
                                    scheduleDAO.createClientNewMembershipRecord(newClient);
                                }

                            }
                        }
                        offSet = offSet + limit;
                    }

                }
            }

        } catch (Exception ex) {
            logger.error("Found error while fetching ClassesAPIActivity exception is {} ", ex);
        }
        return context;
    }
}
