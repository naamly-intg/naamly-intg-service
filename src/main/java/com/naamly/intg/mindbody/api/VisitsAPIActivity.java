package com.naamly.intg.mindbody.api;

import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.db.ClassScheduleRecord;
import com.naamly.intg.mindbody.db.ScheduleDAO;
import com.naamly.intg.mindbody.outbound.VisitsAtGlanceOutBoundController;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VisitsAPIActivity extends BaseActivity {

    private static Logger logger = LoggerFactory.getLogger(ClassesAPIActivity.class);

    @Autowired
    private VisitsAtGlanceOutBoundController visitsAtGlanceOutBoundController;

    private boolean proceedToNextActivity = true;

    @Override
    public boolean proceedToNextActivity() {
        return proceedToNextActivity;
    }

    @Autowired
    private ScheduleDAO scheduleDAO;


    @Override
    public BaseContext execute(BaseContext context) {
        try {
            logger.debug("ScheduleServiceImpl.getClassVisits has been called ");
            StudioContext studioContext = (StudioContext) context;
            String studioId = studioContext.getStudioId();
            List<ClassScheduleRecord> classes = scheduleDAO.getClassesBySiteId(studioId);
            logger.info("Fetched classes from the db size= "+classes.size());
            List<JsonObject> visitsJson = new ArrayList<>();
            for (ClassScheduleRecord record : classes) {
                logger.info("For the classId {} ",record.getClassId());
                String body =
                        visitsAtGlanceOutBoundController.getVisitsInfo(record.getClassId(), studioContext);
                JsonObject visit=studioContext.getGson().fromJson(body, JsonObject.class);
                logger.info("visit is {} ",visit);
                visitsJson.add(visit);
                logger.info("visit is added ");
            }
            logger.info("Total visits is :: {} ",visitsJson.size());
            studioContext.setVisitsJson(visitsJson);
            return context;
        } catch (Exception ex) {
            logger.error("Found error while fetching VisitsAPIActivity exception is {} ", ex);
        }
        return context;
    }
}
