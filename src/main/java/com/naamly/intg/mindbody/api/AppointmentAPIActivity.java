package com.naamly.intg.mindbody.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.db.*;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import com.naamly.intg.mindbody.orchestrator.studio.StudioOrchestrator;
import com.naamly.intg.mindbody.outbound.VisitsAtGlanceOutBoundController;
import com.naamly.intg.mindbody.util.JsonExtractor;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class AppointmentAPIActivity extends BaseActivity {

    private boolean proceedToNextActivity = true;
    private static Logger logger = LoggerFactory.getLogger(StudioOrchestrator.class);

    @Autowired
    private VisitsAtGlanceOutBoundController visitsAtGlanceOutBoundController;

    @Autowired
    private ScheduleDAO scheduleDAO;

    private List<VisitsAtGlanceRecord> recordList=new ArrayList<>();
    @Override
    public BaseContext execute(BaseContext context) {
        StudioContext studioContext = (StudioContext) context;
        try {
            String body =
                    visitsAtGlanceOutBoundController.getAppointments(
                            studioContext.getStartDate(), studioContext.getEndDate(), studioContext);
            if (!Objects.isNull(body)) {
                JsonObject responseJson = studioContext.getGson().fromJson(body, JsonObject.class);
                if (!Objects.isNull(responseJson)) {
                    logger.info("responseJson:: " + responseJson);
                }
                JsonArray json = (JsonArray) responseJson.get("Appointments");
                logger.info("arraysize= {} ",json.size());
                for (int i = 0; i < json.size(); i++) {
                    ClientRecord clientRecord = new ClientRecord();
                    VisitsAtGlanceRecord visitsAtGlanceRecord = new VisitsAtGlanceRecord();
                    visitsAtGlanceRecord.setStudioId(studioContext.getStudioId());
                    JsonObject appointment = (JsonObject) json.get(i);
                    visitsAtGlanceRecord.setClassId(appointment.get("Id").getAsString());

                    if (appointment.get("LocationId") != null) {
                        visitsAtGlanceRecord.setLocationId(appointment.get("LocationId").getAsString());
                    }

                    String startDate = appointment.get("StartDateTime").getAsString();
                    String endDate = appointment.get("EndDateTime").getAsString();
                    String startTime =
                            new SimpleDateFormat("HH:mm:ss").format(studioContext.getFormatter().parse(startDate));
                    String enDTime =
                            new SimpleDateFormat("HH:mm:ss").format(studioContext.getFormatter().parse(endDate));
                    if (startDate != null) {
                        String actualDate = startDate.split("T")[0];
                        visitsAtGlanceRecord.setScheduledDate(studioContext.getSampleDateFormatter().parse(actualDate));
                    }
                    visitsAtGlanceRecord.setStartTime(startTime);
                    visitsAtGlanceRecord.setEndTime(enDTime);
                    String clientId = appointment.get("ClientId").getAsString();

                    JsonObject clientJson = visitsAtGlanceOutBoundController.getClientInfo(clientId, studioContext);
                    if (clientJson == null) {
                        continue;
                    }
                    JsonArray clients = (JsonArray) clientJson.get("Clients");
                    if (clients != null && clients.size() > 0) {
                        clientRecord = JsonExtractor.extractClient(clientId, clients, studioContext);
                    }
                    JsonObject autoPayDetails = visitsAtGlanceOutBoundController.getAutoPayDetails(clientId, studioContext);

                    visitsAtGlanceRecord.setClientId(clientId);
                    ClientAutoPayRecord autoPayRecord = null;
                    if (!Objects.isNull(clientRecord)) {
                        if (autoPayDetails != null) {
                            logger.info("autoPay details= {} ", autoPayDetails);
                            autoPayRecord = JsonExtractor.extractAutoPay(autoPayDetails, clientId, clientRecord.getClientName(), studioContext);
                            if (!Objects.isNull(autoPayRecord)) {
                                scheduleDAO.createClientAutoPayRecord(autoPayRecord);
                            }
                        }
                        JsonObject activeClientMemberShipDetails = visitsAtGlanceOutBoundController.getActiveClientMemberShipDetails(clientId, studioContext);
                        ClientMemberShipRecord clientMemberShipRecord = JsonExtractor.extractMemberShipRecord(clientRecord, activeClientMemberShipDetails, studioContext);
                        clientMemberShipRecord.setRemaining(clientMemberShipRecord.getRemaining());
                        visitsAtGlanceRecord.setRemaining(clientMemberShipRecord.getRemaining());
                        visitsAtGlanceRecord.setClassDescription("Personal Training (APPOINT) / 1:1 Session");
                        visitsAtGlanceRecord.setClientName(clientRecord.getClientName());
                        visitsAtGlanceRecord.setBirthDate(clientRecord.getBirthDate());
                        visitsAtGlanceRecord.setStatus(appointment.get("Status").getAsString());
                        visitsAtGlanceRecord.setEmail(clientRecord.getEmail());
                        visitsAtGlanceRecord.setPhone(clientRecord.getPhoneNumber());
                        visitsAtGlanceRecord.setStaffDetails(appointment.get("StaffId").getAsString());
                        clientMemberShipRecord.setMemberShipType("Active");
                        scheduleDAO.createClientRecord(clientRecord);
                        scheduleDAO.createClientMembershipRecord(clientMemberShipRecord);
                        scheduleDAO.createVisitsAtGlanceRecord(visitsAtGlanceRecord);
                        recordList.add(visitsAtGlanceRecord);
                    }

                }

            }

            logger.info("record list = size {} ",recordList.size());
            logger.info("recordList = "+recordList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return null;
    }

    @Override
    public boolean proceedToNextActivity() {
        return proceedToNextActivity;
    }
}
