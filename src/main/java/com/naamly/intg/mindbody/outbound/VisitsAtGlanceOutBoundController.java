package com.naamly.intg.mindbody.outbound;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.config.ConfigUtils;
import com.naamly.intg.mindbody.config.HttpConfigs;
import com.naamly.intg.mindbody.orchestrator.studio.StudioContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

@Component
public class VisitsAtGlanceOutBoundController {

    private static Logger log = LoggerFactory.getLogger(VisitsAtGlanceOutBoundController.class);

    @Autowired
    private ConfigUtils configUtils;

    @Autowired
    @Qualifier("mindBodyRestTemplate")
    private RestTemplate mindBodyRestTemplate;

    Gson gson=new Gson();

    public JsonObject getClientInfo(String clientId, StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String clientUrl = config.get("clientUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
            uriBuilder.queryParam("ClientIds", clientId);
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);
            if (Objects.isNull(request)) {
                log.debug("ClassesAPIActivity.getClient prepared request is null,so ignore ");
            }
            log.info(" Class URL= {} ", uriBuilder.toUriString());
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            String body = response.getBody();
            if (body == null) {
                return null;
            }
            JsonObject client = studioContext.getGson().fromJson(body, JsonObject.class);
            return client;
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching client info for the client id {} and exception is {} ",
                    clientId,
                    ex);
        }
        return null;
    }

    public String getClassInfo(
            String startDate,
            String endDate,
            boolean isHideCanceledClasses,
            int limit,
            int offSet,
            StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String classUrl = config.get("classesUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(classUrl);
            uriBuilder
                    .queryParam("StartDateTime", startDate)
                    .queryParam("EndDateTime", endDate)
                    .queryParam("HideCanceledClasses", isHideCanceledClasses)
                    .queryParam("Limit", limit)
                    .queryParam("OffSet", offSet);
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeadersWithToken(studioId, studioName, apiKey,studioContext.getStaffToken());
            log.info("uriBuilder {} ", uriBuilder.toUriString());
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            return response.getBody();
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching class info for the startDate {},endDate {}  and exception is {} ",
                    startDate,
                    endDate,
                    ex);
        }
        return null;
    }

    public String getVisitsInfo(String classId, StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String visitorsUrl = config.get("visitorsUrl").getAsString();
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeadersWithToken(studioId, studioName, apiKey,studioContext.getStaffToken());
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(visitorsUrl);
            uriBuilder.queryParam("classId", classId);
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            return response.getBody();

        } catch (Exception ex) {
            log.error(
                    "Found error while fetching visits info for the clientId {} and exception is {} ",
                    classId,
                    ex);
        }
        return null;
    }

    public JsonObject getActiveClientMemberShipDetails(String clientId, StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String clientUrl = config.get("activeClientMemberShipUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
            uriBuilder.queryParam("clientId", clientId);
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);
            if (Objects.isNull(request)) {
                log.debug("ClassesAPIActivity.getClient prepared request is null,so ignore ");
            }
            log.info(" Class URL= {} ", uriBuilder.toUriString());
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            String body = response.getBody();
            if (body == null) {
                return null;
            }
            JsonObject client = studioContext.getGson().fromJson(body, JsonObject.class);
            return client;
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching client info for the client id {} and exception is {} ",
                    clientId,
                    ex);
        }
        return null;
    }

    public JsonObject getAutoPayDetails(String clientId, StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String clientUrl = config.get("autoPayUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
            uriBuilder.queryParam("clientId", clientId);
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);
            if (Objects.isNull(request)) {
                log.debug("ClassesAPIActivity.getClient prepared request is null,so ignore ");
            }
            log.info(" GetAutoPayDetails URL= {} ", uriBuilder.toUriString());
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            String body = response.getBody();
            if (body == null) {
                return null;
            }
            JsonObject client = studioContext.getGson().fromJson(body, JsonObject.class);
            return client;
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching client info for the client id {} and exception is {} ",
                    clientId,
                    ex);
        }
        return null;
    }

    public JsonObject getActiveMemberShipDetails(String clientId, StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String clientUrl = config.get("activeClientMemberShipUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
            uriBuilder.queryParam("clientId", clientId);
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);
            if (Objects.isNull(request)) {
                log.debug("ClassesAPIActivity.getActiveMemberShipDetails prepared request is null,so ignore ");
            }
            log.info(" GetActiveMemberShipDetails URL= {} ", uriBuilder.toUriString());
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            String body = response.getBody();
            if (body == null) {
                return null;
            }
            JsonObject client = studioContext.getGson().fromJson(body, JsonObject.class);
            return client;
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching client info for the client id {} and exception is {} ",
                    clientId,
                    ex);
        }
        return null;
    }

    public String getClientsByCreatedDate(String lastModifiedDate, StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String clientUrl = config.get("clientUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
            uriBuilder.queryParam("LastModifiedDate", lastModifiedDate);
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeadersWithToken(studioId, studioName, apiKey,studioContext.getStaffToken());
            if (Objects.isNull(request)) {
                log.debug("ClassesAPIActivity.getClientsByCreatedDate prepared request is null,so ignore ");
            }
            log.info(" getClientsByCreatedDate URL= {} ", uriBuilder.toUriString());
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            String body = response.getBody();
            if (body == null) {
                return null;
            }
            return body;
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching client info for the lastModifiedDate {} and exception is {} ",
                    lastModifiedDate,
                    ex);
        }
        return null;
    }


    public String getClientsByCreatedDate(String lastModifiedDate,
                                          int limit,
                                          int offSet,
                                          StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String clientUrl = config.get("clientUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
            uriBuilder.queryParam("LastModifiedDate", lastModifiedDate);
            uriBuilder
                    .queryParam("Limit", limit)
                    .queryParam("OffSet", offSet);
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeadersWithToken(studioId, studioName, apiKey,studioContext.getStaffToken());
            if (Objects.isNull(request)) {
                log.debug("ClassesAPIActivity.getClientsByCreatedDate prepared request is null,so ignore ");
            }
            log.info(" getClientsByCreatedDate URL= {} ", uriBuilder.toUriString());
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            String body = response.getBody();
            if (body == null) {
                return null;
            }
            return body;
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching client info for the lastModifiedDate {} and exception is {} ",
                    lastModifiedDate,
                    ex);
        }
        return null;
    }

    public String getAppointments( String startDate,
                                   String endDate,
                                          StudioContext studioContext) {
        try {
            JsonObject config = configUtils.getMBConfiguration();
            String clientUrl = config.get("appointmentsUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
            uriBuilder
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate);
            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpHeadersWithToken(studioId, studioName, apiKey,studioContext.getStaffToken());
            if (Objects.isNull(request)) {
                log.debug("ClassesAPIActivity.getAppointments prepared request is null,so ignore ");
            }
            log.info(" getClientsByCreatedDate URL= {} ", uriBuilder.toUriString());
            ResponseEntity<String> response =
                    mindBodyRestTemplate.exchange(
                            uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
            String body = response.getBody();
            if (body == null) {
                return null;
            }
            return body;
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching client info for the lastModifiedDate {} and exception is {} ",
                    ex);
        }
        return null;
    }

    public String getStaffAccessToken(StudioContext studioContext) {

        try {
            JsonObject config = configUtils.getMBConfiguration();
            String clientUrl = config.get("tokenUrl").getAsString();
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);

            String apiKey = studioContext.getApiKey();
            String studioId = studioContext.getStudioId();
            String studioName = studioContext.getStudioName();
            HttpEntity request = HttpConfigs.getHttpEntityForToken(studioId, studioName, apiKey,studioContext.getStaffUserName(), studioContext.getStaffPwd());
            if (Objects.isNull(request)) {
                log.debug("ClassesAPIActivity.getStaffAccessToken prepared request is null,so ignore ");
            }
            log.info(" getClientsByCreatedDate URL= {} ", uriBuilder.toUriString());
            String response =
                    mindBodyRestTemplate.postForObject(
                            uriBuilder.toUriString(), request, String.class);
            if (response == null) {
                log.info("Outbound Token call been failed empty body from the call ");
                return null;
            }
            JsonObject tokenResponse=gson.fromJson(response,JsonObject.class);
            return tokenResponse.get("AccessToken").getAsString();
        } catch (Exception ex) {
            log.error(
                    "Found error while fetching client info for the lastModifiedDate {} and exception is {} ",
                    ex);
        }
        return null;
    }
}
