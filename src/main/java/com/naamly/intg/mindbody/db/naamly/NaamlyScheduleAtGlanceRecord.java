package com.naamly.intg.mindbody.db.naamly;


import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

public class NaamlyScheduleAtGlanceRecord implements Serializable {

    private BigInteger id;
    private BigInteger clientId;
    private BigInteger trainerId;
    private BigInteger locationId;
    private String startTime;
    private String endTime;
    private String description;
    private String staff;
    private String notes;
    private String clientName;
    private int remaining;
    private String phone;
    private String scheduledOnline;
    private String status;
    private String memberShip;
    private String staffAlert;
    private String yellowAlert;
    private String appointmentNotes;
    private String unpaidAmount;
    private String firstVisit;
    private String birthDay;
    private String lastVisitDate;
    private String nextScheduledDate;
    private String expirationDate;
    private String emailAddress;
    private Timestamp createdAt;


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getClientId() {
        return clientId;
    }

    public void setClientId(BigInteger clientId) {
        this.clientId = clientId;
    }

    public BigInteger getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(BigInteger trainerId) {
        this.trainerId = trainerId;
    }

    public BigInteger getLocationId() {
        return locationId;
    }

    public void setLocationId(BigInteger locationId) {
        this.locationId = locationId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getRemaining() {
        return remaining;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getScheduledOnline() {
        return scheduledOnline;
    }

    public void setScheduledOnline(String scheduledOnline) {
        this.scheduledOnline = scheduledOnline;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberShip() {
        return memberShip;
    }

    public void setMemberShip(String memberShip) {
        this.memberShip = memberShip;
    }

    public String getStaffAlert() {
        return staffAlert;
    }

    public void setStaffAlert(String staffAlert) {
        this.staffAlert = staffAlert;
    }

    public String getYellowAlert() {
        return yellowAlert;
    }

    public void setYellowAlert(String yellowAlert) {
        this.yellowAlert = yellowAlert;
    }

    public String getAppointmentNotes() {
        return appointmentNotes;
    }

    public void setAppointmentNotes(String appointmentNotes) {
        this.appointmentNotes = appointmentNotes;
    }

    public String getUnpaidAmount() {
        return unpaidAmount;
    }

    public void setUnpaidAmount(String unpaidAmount) {
        this.unpaidAmount = unpaidAmount;
    }

    public String getFirstVisit() {
        return firstVisit;
    }

    public void setFirstVisit(String firstVisit) {
        this.firstVisit = firstVisit;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(String lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public String getNextScheduledDate() {
        return nextScheduledDate;
    }

    public void setNextScheduledDate(String nextScheduledDate) {
        this.nextScheduledDate = nextScheduledDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
}
