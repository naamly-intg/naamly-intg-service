package com.naamly.intg.mindbody.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

public class ClientNewMemberShipRecord implements Serializable {

  private String clientId;
  private String clientName;
  private String memberShipName;
  private String phone;
  private Timestamp dateCreated;
  private Timestamp dateJoined;
  private String dueAmount;
  private String monthlyRevenue;
  private Timestamp createdAt;
  private Timestamp lastUpdatedAt;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ClientNewMemberShipRecord that = (ClientNewMemberShipRecord) o;
    return Objects.equals(clientId, that.clientId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(clientId);
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public String getMemberShipName() {
    return memberShipName;
  }

  public void setMemberShipName(String memberShipName) {
    this.memberShipName = memberShipName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Timestamp getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Timestamp dateCreated) {
    this.dateCreated = dateCreated;
  }

  public Timestamp getDateJoined() {
    return dateJoined;
  }

  public void setDateJoined(Timestamp dateJoined) {
    this.dateJoined = dateJoined;
  }

  public String getDueAmount() {
    return dueAmount;
  }

  public void setDueAmount(String dueAmount) {
    this.dueAmount = dueAmount;
  }

  public String getMonthlyRevenue() {
    return monthlyRevenue;
  }

  public void setMonthlyRevenue(String monthlyRevenue) {
    this.monthlyRevenue = monthlyRevenue;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getLastUpdatedAt() {
    return lastUpdatedAt;
  }

  public void setLastUpdatedAt(Timestamp lastUpdatedAt) {
    this.lastUpdatedAt = lastUpdatedAt;
  }
}
