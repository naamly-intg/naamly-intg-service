package com.naamly.intg.mindbody.db.naamly;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class NaamlyAutoExpiration implements Serializable {

   private BigInteger autoExpirationId;
   private BigInteger trainerId;
   private BigInteger locationId;
   private String client;
   private String amount;
   private Date createdAt;
    private String lastAutoPayDate;


    public String getLastAutoPayDate() {
        return lastAutoPayDate;
    }

    public void setLastAutoPayDate(String lastAutoPayDate) {
        this.lastAutoPayDate = lastAutoPayDate;
    }


    public BigInteger getAutoExpirationId() {
        return autoExpirationId;
    }

    public void setAutoExpirationId(BigInteger autoExpirationId) {
        this.autoExpirationId = autoExpirationId;
    }

    public BigInteger getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(BigInteger trainerId) {
        this.trainerId = trainerId;
    }

    public BigInteger getLocationId() {
        return locationId;
    }

    public void setLocationId(BigInteger locationId) {
        this.locationId = locationId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}

