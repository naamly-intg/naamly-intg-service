package com.naamly.intg.mindbody.db.naamly;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

public class NaamlyNewMembersReport implements Serializable {

    private BigInteger id;
    private BigInteger clientId;
    private BigInteger trainerId;
    private String name;
    private String phone;
    private String membershipName;
    private String dateCreated;
    private String dateJoined;
    private String duesAmount;
    private String monthlyRevenue;
    private Timestamp createdDate;
    private BigInteger locationId;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getClientId() {
        return clientId;
    }

    public void setClientId(BigInteger clientId) {
        this.clientId = clientId;
    }

    public BigInteger getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(BigInteger trainerId) {
        this.trainerId = trainerId;
    }

    public BigInteger getLocationId() {
        return locationId;
    }

    public void setLocationId(BigInteger locationId) {
        this.locationId = locationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMembershipName() {
        return membershipName;
    }

    public void setMembershipName(String membershipName) {
        this.membershipName = membershipName;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public String getDuesAmount() {
        return duesAmount;
    }

    public void setDuesAmount(String duesAmount) {
        this.duesAmount = duesAmount;
    }

    public String getMonthlyRevenue() {
        return monthlyRevenue;
    }

    public void setMonthlyRevenue(String monthlyRevenue) {
        this.monthlyRevenue = monthlyRevenue;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
