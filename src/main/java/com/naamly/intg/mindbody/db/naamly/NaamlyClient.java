package com.naamly.intg.mindbody.db.naamly;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

public class NaamlyClient implements Serializable {

    private BigInteger id;
    private String firstName;
    private String lastName;
    private String parentFirstName;
    private String parentLastName;
    private String phone;
    private String gender;
    private String heightUnit;
    private Date dob;
    private String emailId;
    private String mfpUserName;
    private BigInteger trainerId;
    private BigInteger locationId;
    private int dbEnabled;
    private Date startDate;
    private String source;
    private String areaOfInterest;
    private Date terminationDate;
    private String status;
    private int statusOverride;
    private String deleteReason;
    private Timestamp createdDate;
    private double height;

    @Override
    public String toString() {
        return "NaamlyClient{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", parentFirstName='" + parentFirstName + '\'' +
                ", parentLastName='" + parentLastName + '\'' +
                ", phone='" + phone + '\'' +
                ", gender='" + gender + '\'' +
                ", heightUnit='" + heightUnit + '\'' +
                ", dob=" + dob +
                ", emailId='" + emailId + '\'' +
                ", mfpUserName='" + mfpUserName + '\'' +
                ", trainerId=" + trainerId +
                ", locationId=" + locationId +
                ", dbEnabled=" + dbEnabled +
                ", startDate=" + startDate +
                ", source='" + source + '\'' +
                ", areaOfInterest='" + areaOfInterest + '\'' +
                ", terminationDate=" + terminationDate +
                ", status='" + status + '\'' +
                ", statusOverride=" + statusOverride +
                ", deleteReason='" + deleteReason + '\'' +
                ", createdDate=" + createdDate +
                ", height=" + height +
                '}';
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(BigInteger trainerId) {
        this.trainerId = trainerId;
    }

    public BigInteger getLocationId() {
        return locationId;
    }

    public void setLocationId(BigInteger locationId) {
        this.locationId = locationId;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getParentFirstName() {
        return parentFirstName;
    }

    public void setParentFirstName(String parentFirstName) {
        this.parentFirstName = parentFirstName;
    }

    public String getParentLastName() {
        return parentLastName;
    }

    public void setParentLastName(String parentLastName) {
        this.parentLastName = parentLastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHeightUnit() {
        return heightUnit;
    }

    public void setHeightUnit(String heightUnit) {
        this.heightUnit = heightUnit;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMfpUserName() {
        return mfpUserName;
    }

    public void setMfpUserName(String mfpUserName) {
        this.mfpUserName = mfpUserName;
    }

    public int getDbEnabled() {
        return dbEnabled;
    }

    public void setDbEnabled(int dbEnabled) {
        this.dbEnabled = dbEnabled;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAreaOfInterest() {
        return areaOfInterest;
    }

    public void setAreaOfInterest(String areaOfInterest) {
        this.areaOfInterest = areaOfInterest;
    }

    public Date getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusOverride() {
        return statusOverride;
    }

    public void setStatusOverride(int statusOverride) {
        this.statusOverride = statusOverride;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
