package com.naamly.intg.mindbody.db;

import java.io.Serializable;
import java.sql.Timestamp;

public class ClassVisitRecord implements Serializable {

    private String studioId;
    private String locationId;
    private String clientId;
    private String clientName;
    private String phone;
    private String status;
    private String memberShipKind;
    private String appointmentNotes;
    private String isUnPaidAppointment;
    private String isFirstVisit;
    private Timestamp birthDate;
    private String lastUpdatedAt;
    private Timestamp lastVisitedDate;
    private String email;
    private String serviceName;
    private String bookingMethod;
    private String staffName;
    private String remaining;

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getBookingMethod() {
        return bookingMethod;
    }

    public void setBookingMethod(String bookingMethod) {
        this.bookingMethod = bookingMethod;
    }


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getLastVisitedDate() {
        return lastVisitedDate;
    }

    public void setLastVisitedDate(Timestamp lastVisitedDate) {
        this.lastVisitedDate = lastVisitedDate;
    }


    public String getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(String lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    private String isScheduledOnline;
    private String classId;

    public String getIsScheduledOnline() {
        return isScheduledOnline;
    }

    public void setIsScheduledOnline(String isScheduledOnline) {
        this.isScheduledOnline = isScheduledOnline;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getStudioId() {
        return studioId;
    }

    public void setStudioId(String studioId) {
        this.studioId = studioId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberShipKind() {
        return memberShipKind;
    }

    public void setMemberShipKind(String memberShipKind) {
        this.memberShipKind = memberShipKind;
    }

    public String getAppointmentNotes() {
        return appointmentNotes;
    }

    public void setAppointmentNotes(String appointmentNotes) {
        this.appointmentNotes = appointmentNotes;
    }

    public String getIsUnPaidAppointment() {
        return isUnPaidAppointment;
    }

    public void setIsUnPaidAppointment(String isUnPaidAppointment) {
        this.isUnPaidAppointment = isUnPaidAppointment;
    }

    public String getIsFirstVisit() {
        return isFirstVisit;
    }

    public void setIsFirstVisit(String isFirstVisit) {
        this.isFirstVisit = isFirstVisit;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }
}
