package com.naamly.intg.mindbody.db.naamly;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

public class NaamlyMemberShip implements Serializable {

    private BigInteger id;
    private BigInteger trainerId;
    private BigInteger locationId;
    private String name;
    private String status;
    private String membershipType;
    private String memberFrom;
    private String memberTo;
    private String lastAutoPayDate;
    private Timestamp createdDate;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(BigInteger trainerId) {
        this.trainerId = trainerId;
    }

    public BigInteger getLocationId() {
        return locationId;
    }

    public void setLocationId(BigInteger locationId) {
        this.locationId = locationId;
    }

    public String getMemberFrom() {
        return memberFrom;
    }

    public void setMemberFrom(String memberFrom) {
        this.memberFrom = memberFrom;
    }

    public String getMemberTo() {
        return memberTo;
    }

    public void setMemberTo(String memberTo) {
        this.memberTo = memberTo;
    }

    public String getLastAutoPayDate() {
        return lastAutoPayDate;
    }

    public void setLastAutoPayDate(String lastAutoPayDate) {
        this.lastAutoPayDate = lastAutoPayDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
