package com.naamly.intg.mindbody.db;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScheduleDAO {

  @Autowired
  SessionFactory sessionFactory;

  private static final String SELECT_CLASSES_BY_SITE_ID =
      "from ClassScheduleRecord where studioId = :siteId";
  private static final String SELECT_CLIENTS_BY_CLASS_ID =
      "from ClassVisitRecord where class_id= :classId";
  private static final String SELECT_CLIENTS_BY_STUDIO_ID =
          "from ClassVisitRecord where studio_id= :studioId";

  private static final String DELETE_CLASSES_BY_SITE_ID =
          "delete from ClassScheduleRecord where studioId = :siteId";
  private static final String DELETE_VISITS_BY_CLASS_ID =
          "delete from ClassVisitRecord where studioId = :siteId";

  private static final String SELECT_CLIENT_BY_CLIENT_ID =
          "from ClientRecord where client_id= :clientId";

  private static final String SELECT_MEMBERSHIP_BY_CLIENT_ID =
          "from ClientMemberShipRecord where client_id= :clientId";

  private static final String SELECT_LAST_VISIT_BY_CLIENT_ID =
          "from ClientLastVisit where client_id= :clientId";

  private static final String SELECT_AUTO_PAY_BY_CLIENT_ID =
          "from ClientAutoPayRecord where client_id= :clientId";

  public static final Logger logger = LoggerFactory.getLogger(ScheduleDAO.class);

  public boolean createClassesRecord(ClassScheduleRecord record) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.createClassesRecord has been called ");
      sessionObj.beginTransaction();
      sessionObj.saveOrUpdate(record);
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
          "Time taken to ScheduleDAO.createClassesRecord : {}",
          System.currentTimeMillis() - startTime);
    }
    return false;
  }

  public boolean createVisitsAtGlanceRecord(VisitsAtGlanceRecord record) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.createVisitsAtGlanceRecord has been called ");
      sessionObj.beginTransaction();
      sessionObj.saveOrUpdate(record);
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO.createVisitsAtGlanceRecord : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
          "Time taken to ScheduleDAO.createVisitsAtGlanceRecord : {}",
          System.currentTimeMillis() - startTime);
    }
    return false;
  }

  public boolean createClassVisitRecord(ClassVisitRecord record) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.createClassVisitRecord has been called ");
      sessionObj.beginTransaction();
      sessionObj.saveOrUpdate(record);
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO.createClassVisitRecord : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
          "Time taken to ScheduleDAO.createClassVisitRecord : {}",
          System.currentTimeMillis() - startTime);
    }
    return false;
  }

  public List<ClassScheduleRecord> getClassesBySiteId(String siteId) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.getClassesBySiteId has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(SELECT_CLASSES_BY_SITE_ID);
      query.setParameter("siteId", siteId);
      List<ClassScheduleRecord> recordsList = query.list();
      sessionObj.getTransaction().commit();
      return recordsList;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
          "Time taken to ScheduleDAO.getClassesBySiteId : {}",
          System.currentTimeMillis() - startTime);
    }
    return null;
  }

  public List<ClassVisitRecord> getClientsByClassId(String classId) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.getClientsByClassId has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(SELECT_CLIENTS_BY_CLASS_ID);
      query.setParameter("classId", classId);
      List<ClassVisitRecord> recordsList = query.list();
      sessionObj.getTransaction().commit();
      return recordsList;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
          "Time taken to ScheduleDAO.getClientsByClassId : {}",
          System.currentTimeMillis() - startTime);
    }
    return null;
  }

  public boolean deleteClassesBySiteId(String siteId) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.deleteClassesBySiteId has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(DELETE_CLASSES_BY_SITE_ID);
      query.setParameter("siteId", siteId);
      query.executeUpdate();
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.deleteClassesBySiteId : {}",
              System.currentTimeMillis() - startTime);
    }
    return false;
  }
  public boolean deleteVisitsBySiteId(String siteId) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.deleteVisitsBySiteId has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(DELETE_VISITS_BY_CLASS_ID);
      query.setParameter("siteId", siteId);
      query.executeUpdate();
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.deleteVisitsBySiteId : {}",
              System.currentTimeMillis() - startTime);
    }
    return false;
  }

  public boolean createClientRecord(ClientRecord record) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.createClientRecord has been called ");
      sessionObj.beginTransaction();
      sessionObj.saveOrUpdate(record);
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO.createClientRecord : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.createClientRecord : {}",
              System.currentTimeMillis() - startTime);
    }
    return false;
  }
  public boolean createClientMembershipRecord(ClientMemberShipRecord record) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.createClientMembershipRecord has been called ");
      sessionObj.beginTransaction();
      sessionObj.saveOrUpdate(record);
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO.createClientMembershipRecord : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.createClientMembershipRecord : {}",
              System.currentTimeMillis() - startTime);
    }
    return false;
  }

  public boolean createClientNewMembershipRecord(ClientNewMemberShipRecord record) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.createClientNewMembershipRecord has been called ");
      sessionObj.beginTransaction();
      sessionObj.saveOrUpdate(record);
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO.createClientNewMembershipRecord : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.createClientNewMembershipRecord : {}",
              System.currentTimeMillis() - startTime);
    }
    return false;
  }

  public boolean createClientAutoPayRecord(ClientAutoPayRecord record) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.createClientAutoPayRecord has been called ");
      sessionObj.beginTransaction();
      sessionObj.saveOrUpdate(record);
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO.createClientRecord : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.createClientAutoPayRecord : {}",
              System.currentTimeMillis() - startTime);
    }
    return false;
  }

  public ClientAutoPayRecord getClientAutoPayRecord(String clientId ) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.getAllClientsByStudio has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(SELECT_AUTO_PAY_BY_CLIENT_ID);
      query.setParameter("clientId", clientId);
      List<ClientAutoPayRecord> recordsList = query.list();
      sessionObj.getTransaction().commit();
      if(recordsList!=null && recordsList.size()>0){
        return recordsList.get(0);
      }
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO.createClientRecord : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.createClientAutoPayRecord : {}",
              System.currentTimeMillis() - startTime);
    }
    return null;
  }

  public List<ClassVisitRecord> getAllClientVisitsByStudio(String studioId) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.getAllClientsByStudio has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(SELECT_CLIENTS_BY_STUDIO_ID);
      query.setParameter("studioId", studioId);
      List<ClassVisitRecord> recordsList = query.list();
      sessionObj.getTransaction().commit();
      return recordsList;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.getClientsByClassId : {}",
              System.currentTimeMillis() - startTime);
    }
    return null;
  }

  public boolean createLastVisitRecord(ClientLastVisitRecord record) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.createLastVisitRecord has been called ");
      sessionObj.beginTransaction();
      sessionObj.saveOrUpdate(record);
      sessionObj.getTransaction().commit();
      return true;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO.createLastVisitRecord : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.createLastVisitRecord : {}",
              System.currentTimeMillis() - startTime);
    }
    return false;
  }

  public ClientRecord getClientByClientId(String clientId) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.getClientByClientId has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(SELECT_CLIENT_BY_CLIENT_ID);
      query.setParameter("clientId", clientId);
      List<ClientRecord> recordsList = query.list();
      sessionObj.getTransaction().commit();
      if(recordsList!=null && recordsList.size()>0){
        return recordsList.get(0);
      }
      return null;
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.getClientByClientId : {}",
              System.currentTimeMillis() - startTime);
    }
    return null;
  }

  public ClientMemberShipRecord getMembershipByClientId(String clientId) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.getClientsByClassId has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(SELECT_MEMBERSHIP_BY_CLIENT_ID);
      query.setParameter("clientId", clientId);
      List<ClientMemberShipRecord> recordsList = query.list();
      sessionObj.getTransaction().commit();
      if(recordsList!=null && recordsList.size()>0){
        return recordsList.get(0);
      }
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.getClientsByClassId : {}",
              System.currentTimeMillis() - startTime);
    }
    return null;
  }

  public ClientLastVisitRecord getLastVisitRecordClientId(String clientId) {
    long startTime = System.currentTimeMillis();
    Session sessionObj = sessionFactory.openSession();
    try {
      logger.debug("ScheduleDAO.getClientsByClassId has been called ");
      sessionObj.beginTransaction();
      Query query = sessionObj.createQuery(SELECT_LAST_VISIT_BY_CLIENT_ID);
      query.setParameter("clientId", clientId);
      List<ClientLastVisitRecord> recordsList = query.list();
      sessionObj.getTransaction().commit();
      return recordsList.get(0);
    } catch (Exception ex) {
      sessionObj.getTransaction().rollback();
      logger.error("Found Exception while ScheduleDAO : {} ", ex);
    } finally {
      if (sessionObj != null) {
        sessionObj.close();
      }
      logger.info(
              "Time taken to ScheduleDAO.getClientsByClassId : {}",
              System.currentTimeMillis() - startTime);
    }
    return null;
  }
}
