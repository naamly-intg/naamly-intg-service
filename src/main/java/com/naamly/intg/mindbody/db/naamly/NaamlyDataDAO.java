package com.naamly.intg.mindbody.db.naamly;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.naamly.intg.mindbody.db.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.util.List;

@Component
public class NaamlyDataDAO {

    @Autowired
    SessionFactory sessionFactory;


    public static final Logger logger = LoggerFactory.getLogger(NaamlyDataDAO.class);

    public static String CLIENT_INSERT = "insert into client(id,first_name,last_name,phone,dob,email_id,trainer_id,location_id,status,height_unit,db_enabled) " +
            "values (?,?,?,?,?,?,?,?,?,?,?)";

    public static String SCHEDULE_INSERT = "insert into schedule_at_glance(id,first_name,last_name,phone,dob,email_id,trainer_id,location_id,status,height_unit,db_enabled) " +
            "values (?,?,?,?,?,?,?,?,?,?,?)";

    public static String CLIENT_BY_PHONE_AND_NAMES_FETCH = "from NaamlyClient  where first_name= :firstName and last_name= :lastName";

    public void saveScheduleGlanceRecord(NaamlyScheduleAtGlanceRecord record) {
        long startTime = System.currentTimeMillis();
        Session sessionObj = sessionFactory.openSession();
        try {
            logger.debug("NaamlyDataDAO.saveScheduleGlanceRecord has been called {} ", record);
            sessionObj.beginTransaction();
            sessionObj.saveOrUpdate(record);
            sessionObj.getTransaction().commit();
        } catch (Exception ex) {
            sessionObj.getTransaction().rollback();
            logger.error("Found Exception while NaamlyDataDAO : {} ", ex);
        } finally {
            if (sessionObj != null) {
                sessionObj.close();
            }
            logger.info(
                    "Time taken to NaamlyDataDAO.saveScheduleGlanceRecord : {}",
                    System.currentTimeMillis() - startTime);
        }
    }

    public BigInteger saveClientRecord(NaamlyClient record) {
        long startTime = System.currentTimeMillis();
        Session sessionObj = sessionFactory.openSession();
        try {
            logger.debug("NaamlyDataDAO.saveClientRecord has been called {} ", record);
            sessionObj.beginTransaction();
            sessionObj.save(record);
           /* Query query = sessionObj.createSQLQuery(CLIENT_INSERT);
            query.setParameter(0, record.getId());
            query.setParameter(1, record.getFirstName());
            query.setParameter(2, record.getLastName());
            query.setParameter(3, record.getPhone());
            query.setParameter(4, record.getDob());
            query.setParameter(5, record.getEmailId());
            query.setParameter(6, record.getTrainerId());
            query.setParameter(7, record.getLocationId());
            query.setParameter(8, record.getStatus());
            query.setParameter(9, record.getHeightUnit());
            query.setParameter(10, 1);
            query.executeUpdate();*/
            sessionObj.getTransaction().commit();
        } catch (Exception ex) {
            sessionObj.getTransaction().rollback();
            logger.error("Found Exception while NaamlyDataDAO.saveClientRecord : {} ", ex);
        } finally {
            if (sessionObj != null) {
                sessionObj.close();
            }
            logger.info(
                    "Time taken to NaamlyDataDAO.saveClientRecord : {}",
                    System.currentTimeMillis() - startTime);
        }
        return null;
    }

    public void saveMemberShip(NaamlyMemberShip record) {
        long startTime = System.currentTimeMillis();
        Session sessionObj = sessionFactory.openSession();
        try {
            logger.debug("NaamlyDataDAO.saveMemberShip has been called {}  ", record);
            sessionObj.beginTransaction();
            sessionObj.saveOrUpdate(record);
            sessionObj.getTransaction().commit();
        } catch (Exception ex) {
            sessionObj.getTransaction().rollback();
            logger.error("Found Exception while NaamlyDataDAO.saveMemberShip : {} ", ex);
        } finally {
            if (sessionObj != null) {
                sessionObj.close();
            }
            logger.info(
                    "Time taken to saveMemberShip : {}",
                    System.currentTimeMillis() - startTime);
        }
    }

    public BigInteger saveNewMemberShip(NaamlyNewMembersReport record) {
        long startTime = System.currentTimeMillis();
        Session sessionObj = sessionFactory.openSession();
        try {
            logger.debug("NaamlyDataDAO.saveNewMemberShip has been called {} ", record);
            sessionObj.beginTransaction();
            sessionObj.saveOrUpdate(record);
            sessionObj.getTransaction().commit();
            return record.getId();
        } catch (Exception ex) {
            sessionObj.getTransaction().rollback();
            logger.error("Found Exception while NaamlyDataDAO.saveNewMemberShip : {} ", ex);
        } finally {
            if (sessionObj != null) {
                sessionObj.close();
            }
            logger.info(
                    "Time taken to saveMemberShip : {}",
                    System.currentTimeMillis() - startTime);
        }
        return null;
    }

    public void saveCoachingHours(NaamlyCoachingHours record) {
        long startTime = System.currentTimeMillis();
        Session sessionObj = sessionFactory.openSession();
        try {
            logger.debug("NaamlyDataDAO.saveCoachingHours has been called {}  ", record);
            sessionObj.beginTransaction();
            sessionObj.saveOrUpdate(record);
            sessionObj.getTransaction().commit();
        } catch (Exception ex) {
            sessionObj.getTransaction().rollback();
            logger.error("Found Exception while NaamlyDataDAO.saveCoachingHours : {} ", ex);
        } finally {
            if (sessionObj != null) {
                sessionObj.close();
            }
            logger.info(
                    "Time taken to saveMemberShip : {}",
                    System.currentTimeMillis() - startTime);
        }
    }

    public void saveAutoExpiration(NaamlyAutoExpiration record) {
        long startTime = System.currentTimeMillis();
        Session sessionObj = sessionFactory.openSession();
        try {
            logger.info("NaamlyDataDAO.saveAutoExpiration has been called {} ", record);
            sessionObj.beginTransaction();
            sessionObj.saveOrUpdate(record);
            sessionObj.getTransaction().commit();
        } catch (Exception ex) {
            sessionObj.getTransaction().rollback();
            logger.error("Found Exception while NaamlyDataDAO.saveAutoExpiration : {} ", ex);
        } finally {
            if (sessionObj != null) {
                sessionObj.close();
            }
            logger.info(
                    "Time taken to saveAutoExpiration : {}",
                    System.currentTimeMillis() - startTime);
        }
    }

    public NaamlyClient getNaamlyClientByPhoneAndName(String firstName, String lastName) {
        long startTime = System.currentTimeMillis();
        Session sessionObj = sessionFactory.openSession();
        try {
            logger.debug("NaamlyDataDAO.getNaamlyClientByPhoneAndName has been called phone {},firstName {} ,lastName {} ", firstName, lastName);
            sessionObj.beginTransaction();
            Query query = sessionObj.createQuery(CLIENT_BY_PHONE_AND_NAMES_FETCH);
            query.setParameter("firstName", firstName);
            query.setParameter("lastName", lastName);
            List<NaamlyClient> recordsList = query.list();
            if (recordsList != null && recordsList.size() > 0) {
                logger.info("found record {} ", recordsList.get(0));
                return recordsList.get(0);
            }
        } catch (Exception ex) {
            sessionObj.getTransaction().rollback();
            logger.error("Found Exception while NaamlyDataDAO.saveCoachingHours : {} ", ex);
        } finally {
            if (sessionObj != null) {
                sessionObj.close();
            }
            logger.info(
                    "Time taken to getNaamlyClientByPhoneAndName : {}",
                    System.currentTimeMillis() - startTime);
        }
        return null;
    }
}
